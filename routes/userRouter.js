const router = require('express').Router()
const userCtrl = require('../controllers/userCtrl')
const adminCtrl = require('../controllers/adminCtrl')
const ambassadorCtrl = require('../controllers/ambassadorCtrl')
const restaurantCtrl = require('../controllers/restaurantCtrl')
const auth = require('../middleware/auth')
const authAdmin = require('../middleware/authAdmin')

router.post('/register', userCtrl.register)

router.post('/activation', userCtrl.activateEmail)

router.post('/login', userCtrl.login)

router.post('/refresh_token', userCtrl.getAccessToken)

router.post('/forgot', userCtrl.forgotPassword)

router.post('/reset', auth, userCtrl.resetPassword)

router.get('/infor', auth, userCtrl.getUserInfor)

router.get('/all_infor', auth, authAdmin, userCtrl.getUsersAllInfor)

router.post('/logout', userCtrl.logout)

router.patch('/update', auth, userCtrl.updateUser)

router.patch('/update_role/:id', auth, authAdmin, userCtrl.updateUsersRole)

router.delete('/delete/:id', auth, authAdmin, userCtrl.deleteUser)

router.post('/ambassador_request/:id', auth, userCtrl.ambassadorRequest)

router.post('/send_refferal/:id', auth, ambassadorCtrl.sendReferral)

router.get('/get_ambassador_request', auth, adminCtrl.getAllAmbassadorRequest)

// restaurant apis
router.post('/add-restaurant', auth, restaurantCtrl.addRestaurant)

router.post('/update-restaurant', auth, restaurantCtrl.updateRestaurant)

router.get('/restaurant-list', auth, restaurantCtrl.getRestaurants)

router.post('/delete-restaurant', auth, restaurantCtrl.deleteRestaurant)

router.get('/restaurants', restaurantCtrl.getRestaurants)

// Social Login
router.post('/google_login', userCtrl.googleLogin)

router.post('/facebook_login', userCtrl.facebookLogin)


router.get('/search', auth, userCtrl.searchUser)

router.get('/user/:id', auth, userCtrl.getUser)

router.patch('/user', auth, userCtrl.updateUser)

router.patch('/user/:id/follow', auth, userCtrl.follow)
router.patch('/user/:id/unfollow', auth, userCtrl.unfollow)

router.get('/suggestionsUser', auth, userCtrl.suggestionsUser)



module.exports = router