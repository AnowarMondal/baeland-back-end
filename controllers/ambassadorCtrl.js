const Users = require('../models/userModel')
const Ambassador = require('../models/ambassadorModel')
const Refferal = require('../models/refferalModel')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const sendRefferal = require('./sendRefferal')
const fetch = require('node-fetch')

const CLIENT_URL = process.env.CLIENT_URL;
const ambassadorCtrl = {
   
    sendReferral: async (req, res) => {
        try {
            const {email} = req.body
            const id = req.params.id;
            
            if(!validateEmail(email))
                return res.status(400).json({msg: "Invalid emails."})


            const newUser = {
                user: id ,email
            }

            const refferal_token = createRefferalToken(newUser)

            const url = `${CLIENT_URL}/users/activate/${refferal_token}`
            sendRefferal(email, url, "Refferal link for BeaLand")
            
            const newRefferal = new Refferal({
                user:id,email
            })
            await newRefferal.save()
            res.json({msg: "Refferal Request Success"})

        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    getUsersAllInfor: async (req, res) => {
        try {
            const users = await Users.find().select('-password')

            res.json(users)
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    updateUser: async (req, res) => {
        try {
            const {name, avatar} = req.body
            await Users.findOneAndUpdate({_id: req.user.id}, {
                name, avatar
            })

            res.json({msg: "Update Success!"})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    updateUsersRole: async (req, res) => {
        try {
            const {role} = req.body

            await Users.findOneAndUpdate({_id: req.params.id}, {
                role
            })

            res.json({msg: "Update Success!"})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    deleteUser: async (req, res) => {
        try {
            await Users.findByIdAndDelete(req.params.id)

            res.json({msg: "Deleted Success!"})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    getAllReffaral : async (req, res) => {
        
        try {
            const reffaral = await Reffaral.findById(id)
            res.json(reffaral)
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
   
}





function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

const createActivationToken = (payload) => {
    return jwt.sign(payload, process.env.ACTIVATION_TOKEN_SECRET, {expiresIn: '5m'})
}

const createAccessToken = (payload) => {
    return jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {expiresIn: '15m'})
}

const createRefreshToken = (payload) => {
    return jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {expiresIn: '7d'})
}

const createRefferalToken = (payload) => {
    return jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {expiresIn: '30d'})
}

module.exports = ambassadorCtrl