const yup = require("yup");
const Users = require("../models/userModel");
const Restaurant = require("../models/restaurantModel");
const fetch = require("node-fetch");

let AddRestaurantSchema = yup.object().shape({
  category: yup.string().required(),
  price: yup.number().required().positive().integer(),
  discount: yup.number().required().positive().integer(),
  description: yup.string().required(),
  image: yup.string()
});


let UpdateRestaurantSchema = yup.object().shape({
  id: yup.string().required(),
  category: yup.string().required(),
  price: yup.number().required().positive().integer(),
  discount: yup.number().required().positive().integer(),
  description: yup.string().required(),
  image: yup.string()
});

let DeleteRestaurantSchema = yup.object().shape({
  id: yup.string().required()
});


const restaurantCtrl = {
  addRestaurant: async (req, res) => {
    try {
      await AddRestaurantSchema.validate(req.body, { abortEarly: false });
      const { category, price, discount, description, image } = req.body;

      const newRestaurant = new Restaurant({
        category,
        price,
        discount,
        description,
        image
      })

      await newRestaurant.save();

      res.json({ msg: "Restaurant sucessfully added" });
    } catch (err) {
      if (err.name.includes("ValidationError")) {
        return res.status(400).json({ msg: err.name, errors: err.errors });
      }
      return res.status(500).json({ msg: err.message });
    }
  },

  getRestaurants: async (req,res) => {
    try {

        const restaurants = await Restaurant.find();

        res.json({ msg: "Restaurant list", data: restaurants });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  updateRestaurant: async (req,res) => {
    try {
       await UpdateRestaurantSchema.validate(req.body, { abortEarly: false });
       const { id, category, price, discount, description, image } = req.body;
        
        const updateData = {category, price, discount, description}
        if(image) updateData.image = image
        const restaurant = await Restaurant.findOneAndUpdate({_id: id},updateData);

        res.json({ msg: "Restaurant updated", data: restaurant });
    } catch (err) {
      if (err.name.includes("ValidationError")) {
        return res.status(400).json({ msg: err.name, errors: err.errors });
      }
      return res.status(500).json({ msg: err.message });
    }
  },
  deleteRestaurant: async (req,res) => {
    try {
        await DeleteRestaurantSchema.validate(req.body, { abortEarly: false });
        const { id } = req.body;
        await Restaurant.findByIdAndDelete(id);

        res.json({ msg: "Restaurant deleted"});
    } catch (err) {
      if (err.name.includes("ValidationError")) {
        return res.status(400).json({ msg: err.name, errors: err.errors });
      }
      return res.status(500).json({ msg: err.message });
    }
  },
};

module.exports = restaurantCtrl;
