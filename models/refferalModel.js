const mongoose = require('mongoose')

const refferalSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    status:{
        type:Number,
        default:0
    },
    user: {type: mongoose.Types.ObjectId, ref: 'user'},
}, {
    timestamps: true
})

module.exports = mongoose.model('refferal', refferalSchema)