const mongoose = require('mongoose')

const ambassadorSchema = new mongoose.Schema({
    message: {
        type: String,
        required: true
    },
    status:{
        type:Number,
        default:0
    },
    user: {type: mongoose.Types.ObjectId, ref: 'user'},
}, {
    timestamps: true
})

module.exports = mongoose.model('ambassador', ambassadorSchema)