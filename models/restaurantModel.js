const mongoose = require('mongoose')

const restaurantSchema = new mongoose.Schema({
    category: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price:{
        type:Number,
        default:0
    },
    discount:{
        type:Number,
        default:0
    },
    image: {
        type: String,
        required: false,
    },
    user: {type: mongoose.Types.ObjectId, ref: 'user'},
}, {
    timestamps: true
})

module.exports = mongoose.model('restaurant', restaurantSchema)