const Users = require('../models/userModel')

const authAmbassador = async (req, res, next) => {
    try {
        const user = await Users.findOne({_id: req.user.id})

        if(user.role !== 2) 
            return res.status(500).json({msg: "Ambassador resources access denied."})

        next()
    } catch (err) {
        return res.status(500).json({msg: err.message})
    }
}

module.exports = authAmbassador